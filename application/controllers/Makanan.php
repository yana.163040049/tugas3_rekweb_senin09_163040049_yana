<?php

require APPPATH . '/libraries/REST_Controller.php';

class Makanan extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Makanan_model", "makanan");
    }

    public function index_get()
    {
        // ketikan source code yang ada di modul
        $id = $this->get('id');
        $cari = $this->get('cari');

        if ($cari !="") {
            # code...
            $makanan = $this->makanan->cari($cari)->result();

        }else if ($id == '') {
            # code...
            $makanan = $this->makanan->cari(null)->result();

        }else{
            $makanan = $this->makanan->getData($id)->result();
        }
        $this->response($makanan);
    }

    public function index_put()
    {
        // ketikan source code yang ada di modul
        $nama = $this->put('nama');
        $deskripsi = $this->put('deskripsi');
        $jenis = $this->put('jenis');
        $harga = $this->put('harga');
        $stok = $this->put('stok');
        $gambar = $this->put('gambar');
        $id= $this->put('id');
        $data = array(
            'nama'  => $nama,
            'deskripsi' => $deskripsi,
            'jenis' => $jenis,
            'harga' => $harga,
            'stok' => $stok,
            'gambar' => $gambar,


        );
        $update = $this->makanan->update('makanan', $data, 'id', $this->put('id') );
        if ($update) {
            # code...
            $this->response(array('status' => 'success', 200));

        }else{
            $this->response(array('status'=>'fail', 502));

        }
    }

    public function index_post()
    {


        $nama = $this->post('nama');
        $deskripsi = $this->post('deskripsi');
        $jenis = $this->post('jenis');
        $harga = $this->post('harga');
        $stok = $this->post('stok');
        $gambar = $this->post('gambar');

        $data = array(
            'nama'  => $nama,
            'deskripsi' => $deskripsi,
            'jenis' => $jenis,
            'harga' => $harga,
            'stok' => $stok,
            'gambar' => $gambar,


        );

        $insert = $this->makanan->insert($data);
        if ($insert) {
            # code...
            $this->response(array('status' => 'success', 200));

        }else{
            $this->response(array('status'=> 'fail', 502));

        }

    }

    public function index_delete()
    {
       // ketikan source code yang ada di modul
        $id = $this->delete('id');
        // $this->db->where('id', $id);
        $delete = $this->makanan->delete('makanan', 'id', $id);
        if($delete){
              $this->response(array('status' => 'success', 200));

        }else{
            $this->response(array('status'=> 'fail', 502));
        }
    }
}