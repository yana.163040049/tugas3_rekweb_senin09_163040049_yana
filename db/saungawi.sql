-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2018 at 05:13 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saungawi`
--

-- --------------------------------------------------------

--
-- Table structure for table `makanan`
--

CREATE TABLE `makanan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `jenis` varchar(7) NOT NULL,
  `harga` int(12) NOT NULL,
  `stok` int(100) NOT NULL,
  `gambar` varchar(20) DEFAULT '3.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `makanan`
--

INSERT INTO `makanan` (`id`, `nama`, `deskripsi`, `jenis`, `harga`, `stok`, `gambar`) VALUES
(19, 'Nasi Liwet', 'Nasi Liwet merupakan makanan yang sangat sering disajikan saat acara kumpul-kumpul. Nasi Liwet juga akrab disajikan dengan tambahan ikan teri, daun serai, tahu, dan juga ayam goreng.', 'makanan', 120000, 2, '5c0bdc1035150.jpg'),
(20, 'Nasi Timbel', 'Sajian dari Nasi Timbel sendiri agak mirip dengan sajian dari Nasi Liwet. Hanya saja, Nasi Timbel umumnya disajikan dengan bungkusan daun pisang.', 'makanan', 15000, 20, '5c0bce25387fd.jpg'),
(21, 'Nasi Tutug Oncom', 'Mungkin masih banyak yang belum pernah mendengar mengenai Nasi Tutug Oncom. Hidangan yang berasal dari Tasikmalaya ini merupakan campuran dari nasi dan juga olahan kedelai alias oncom.\r\n\r\n\r\nTidak jarang bagi restoran-restoran untuk menyajikan Nasi Tutug Oncom dengan tambahan ikan asin, daun pisang, jeruk nipis, ataupun tempe. Bahan saus yang membuat hidangan ini unik adalah karena sambal hijaunya.', 'makanan', 25000, 5, '5c0bd1202b262.jpg'),
(22, 'Lotek', 'Ini dia makanan hidangan yang hampir sama dengan Karedok. Sama-sama menggunakan bumbu kacang dan kerupuk, Lotek lebih menyajikan sayur-sayuran rebus seperti bayam, kapri, dan kacang panjang.', 'makanan', 27000, 7, '5c0bdc774062a.jpg'),
(23, 'Bakakak Hayam', 'Bakakak Hayam adalah masakan khas Sunda yang unik meski baru hanya melihat cara penyajiannya. umumnya Bakakak Hayam menggunakan dada ayam yang dibelah hingga terbuka dan ditusukan hingga membentuk sebuah bakakak.', '45', 18000, 8, '5c0bd9a1ae7a5.jpg'),
(24, 'Soto Bandung', 'Jika Toppers ingin mencoba masakan khas Sunda yang minimalis, Soto Bandung adalah pilihannya. Soto Bandung umumnya menggunakan daging sapi has dalam atau tetelan.\r\n\r\nYang membedakan Soto Bandung dari yang lainnya adalah penambahan kacang kedelai goreng dan juga lobak di kuahnya.', 'makanan', 12000, 6, '5c0bd9f3bed72.jpg'),
(25, 'Soto Mie', 'Soto Mie bukanlah merupakan nama masakan yang jarang terdengar. Kemanapun kotanya, Toppers tetap dapat menemukan Soto Mie. Soto Mie sendiri adalah jenis masakan khas Sunda yang menyajikan mie dengan kuah kaldu yang kental.\r\n\r\nUmumnya, Soto Mie menggunakan tambahan tauge dan kacang kedelai goreng untuk menambahkan sedikit rasa manis.', 'makanan', 45000, 5, '5c0bda276d963.jpg'),
(26, 'Sate Maranggi', 'Makanan Sunda kali ini ketenarannya memang sudah mendunia. Sate Maranggi yang khas menggunakan daging sapi atau ayam ini sebenarnya mempunyai asal usul yang cukup menarik.\r\n\r\nAwalnya, Sate Maranggi adalah makanan asing dari para pendatang China yang menetap di Indonesia. Daging yang awalnya digunakan pun adalah daging babi. Makanan ini juga mempunyai cita rasa yang sangat kuat akibat cara pemasakannya. Daging biasanya direndam di bumbu sebelum dibakar.', 'makanan', 8000, 78, '5c0bda81a22cd.jpg'),
(27, 'Empal Gentong', 'Berbeda dari empal lainnya, Empal Gentong dimasak dengan menggunakan kayu bakar dan gentong. Bahan utamanya juga berupa daging, usus, dan babat sapi.\r\n\r\nEmpal Gentong dihidangkan dengan nasi atau lontong, dan kucai. Empal Gentong juga terasa nikmat jika dimakan dengan kuah santan.', 'makanan', 67000, 67, '5c0bdae932c6f.jpg'),
(28, ' Surabi', 'Surabi adalah semacam kue pancake yang sebagian besarnya menggunakan tepung beras dan santan. Kue tradisional Sunda ini sangat populer di kota Bandung. Awalnya, Surabi hanya disajikan dengan tambahan irisan kelapa, tapi sekarang sudah banyak yang mencampurkan Surabi dengan coklat, vanila, dan bahkan oncom.', 'makanan', 23000, 78, '5c0bdb249adaa.jpg'),
(29, 'Tahu Sumedang', 'Kuliner khas Sunda selanjutnya adalah Tahu Sumedang. Berbeda dari tahu goreng biasa, Tahu Sumedang mempunyai tekstur yang khas. Ketika dimakan, kulit Tahu Sumedang yang renyah dan kering bercampur dengan tekstur dalamnya yang sangat lembut.\r\n\r\nNamun, untuk menikmati sensasi makan yang maksimal, Tahu Sumedang sebaiknya dimakan ketika masih hangat. Kulit Tahu Sumedang yang ditinggal hingga dingin tidak lagi akan terasa renyah.', '78', 67000, 89, '5c0bdb5466941.jpg'),
(30, 'Tahu Gejrot', 'Satu lagi tahu khas Sunda yang terkenal di kalangan masakan khas Sunda adalah Tahu Gejrot. Tahu Gejrot ini berasal dari Cirebon. Umumnya, Tahu Gejrot dimakan dengan kuah pedas.\r\n\r\nBerbeda dari Tahu Sumedang yang isinya penuh, Tahu Gejrot mempunyai isi yang agak kosong. Tujuannya adalah supaya kuah dan bumbu-bumbu cair lainnya dapat terserap ke dalam tahu lebih cepat.', '90', 9000, 89, '5c0bdbaa0fcc2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jenis kelamin` char(1) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `makanan`
--
ALTER TABLE `makanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `makanan`
--
ALTER TABLE `makanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
